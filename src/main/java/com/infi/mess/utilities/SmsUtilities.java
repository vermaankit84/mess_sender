package com.infi.mess.utilities;

import com.infi.mess.beans.Message;
import com.infi.mess.beans.Vendor;
import com.infi.mess.constants.RequestMode;
import com.infi.mess.exception.SenderException;
import com.infi.mess.httpclient.HttpClient;
import org.apache.log4j.Logger;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class SmsUtilities {

    private static SmsUtilities shared = null;
    private Logger logger = Logger.getLogger(SmsUtilities.class.getName());

    private SmsUtilities() {
    }

    public static synchronized SmsUtilities getShared() {
        if (shared == null) {
            shared = new SmsUtilities();
        }
        return shared;
    }

    public int getResponse(final Message messageData, final List<Vendor> activeVendorList) throws SenderException, IOException {
        if (activeVendorList == null || activeVendorList.isEmpty()) {
            throw new SenderException("active vendor list is cannot be empty");
        }
        int strResponse = 0;
        for (final Vendor vendor : activeVendorList) {
            final RequestMode requestMode = vendor.getRequestMode();
            logger.info("Trying to send message [ " + messageData + " ] at [ " + Utilities.getCurrentDateTime() + " ] by vendor [ " + vendor.getVendorName() + " ] ");
            switch (requestMode) {
                case GET:
                    final String primaryUrl = vendor.getVendorUrl();
                    final String strUrl = buildGetUrl(messageData, primaryUrl);
                    strResponse = HttpClient.getShared().getGetResponse(strUrl);
                    if (!StringUtils.isEmpty(strResponse)) {
                        logger.info("Url to hit obtained [ " + strUrl + " ] at [ " + Utilities.getCurrentDateTime() + " ] Response Obtained [ " + strResponse + " ] ");
                    }
                    break;
                case POST:
                    final Map<String, MultiValueMap<String, String>> multiValueMap = buildListNameValuePair(messageData, vendor);
                    final Iterator<String> iterator = multiValueMap.keySet().iterator();
                    if (iterator.hasNext()) {
                        final String key = iterator.next();
                        final MultiValueMap<String, String> m = multiValueMap.get(key);
                        strResponse = HttpClient.getShared().getPostResponse(key, m, vendor.getVendorHeader());
                        if (!StringUtils.isEmpty(strResponse)) {
                            logger.info("Url to hit obtained [ " + key + " ] at [ " + Utilities.getCurrentDateTime() + " ] Response Obtained [ " + strResponse + " ] ");
                        }
                    }
                    break;
                default:
                    throw new UnsupportedOperationException("This mode [ " + requestMode + " ] is not supported ");
            }
            if (strResponse != 0) {
                break;
            }
        }
        return strResponse;
    }

    private String buildGetUrl(final Message messageData, final String primaryUrl) {
        return primaryUrl.replaceAll("%message%", messageData.getMessage()).replaceAll("%phone%", messageData.getDestination());
    }

    private Map<String, MultiValueMap<String, String>> buildListNameValuePair(final Message messageData, final Vendor vendor) {
        final Map<String, MultiValueMap<String, String>> buildListNameValueMap = new HashMap<>();
        final String vendorCredentials = vendor.getVendorCredentials();
        final String strUrl = vendor.getVendorUrl().split(",")[0];
        final String messageUrl = vendor.getVendorUrl().split(",")[1];
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add(vendorCredentials.split(",")[0].split("=")[0], vendorCredentials.split(",")[0].split("=")[1]);
        map.add(vendorCredentials.split(",")[1].split("=")[0], vendorCredentials.split(",")[1].split("=")[1]);
        map.add(messageUrl.split("&")[0].split("=")[0], messageData.getDestination());
        map.add(messageUrl.split("&")[1].split("=")[0], messageData.getMessage());
        buildListNameValueMap.put(strUrl, map);
        return buildListNameValueMap;
    }
}
