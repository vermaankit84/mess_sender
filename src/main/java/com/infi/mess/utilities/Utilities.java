package com.infi.mess.utilities;


import java.time.LocalTime;

public class Utilities {
    private Utilities() {
    }

    public static String getCurrentDateTime() {
        return LocalTime.now().toString();
    }
}
