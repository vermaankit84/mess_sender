package com.infi.mess.repository;


import com.infi.mess.beans.Vendor;
import com.infi.mess.exception.SenderException;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("vendorRepository")
public interface VendorRepository extends CrudRepository<Vendor, Integer>, JpaSpecificationExecutor<Vendor> {

    @Query("select v from Vendor v where v.vendorName = :vendorName order by v.vendorPriority")
    public Vendor getVendorDetailsBasedOnName(@Param("vendorName") final String vendorName) throws SenderException;
}
