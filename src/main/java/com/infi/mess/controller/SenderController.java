package com.infi.mess.controller;

import com.infi.mess.beans.Message;
import com.infi.mess.exception.SenderException;
import com.infi.mess.service.SenderService;
import com.infi.mess.utilities.Utilities;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class SenderController {

    private final Logger logger = Logger.getLogger(SenderController.class.getName());
    @Autowired
    private SenderService senderService = null;

    @RequestMapping(value = "/sendMessage", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<String> sendMessage(@RequestBody final Message message) throws SenderException {
        if (message == null) {
            throw new SenderException("Message data cannot be empty");
        }

        ResponseEntity<String> responseEntity = null;
        logger.info(" Message [ " + message + " ] obtained at [ " + Utilities.getCurrentDateTime() + " ] ");

        try {
            final boolean isMessageSend = senderService.sendMessage(message) == 201;
            final String strCode =  isMessageSend ? "Message [ " + message + " ]  has send successfully"
                    : "Message [ " + message + " ] has not send successfully";
            logger.info(" [ " + strCode + " ] at [ " + Utilities.getCurrentDateTime() + " ] ");
            responseEntity =  new ResponseEntity<>(strCode, HttpStatus.CREATED);
        } catch (SenderException | IOException e) {
            logger.warn("Exception arises while sending message [ " + message + " ] at [ " + Utilities.getCurrentDateTime() + " ] " , e);
            responseEntity =  new ResponseEntity<>("Exception arises while sending message [ " + message + " ] ", HttpStatus.FORBIDDEN);
        }
        return responseEntity;
    }
}
