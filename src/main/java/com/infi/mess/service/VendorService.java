package com.infi.mess.service;


import com.infi.mess.beans.Vendor;
import com.infi.mess.exception.SenderException;
import com.infi.mess.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service("vendorService")
@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
@Cacheable(cacheNames = "vendorCache")
public class VendorService {

    @Autowired
    private VendorRepository vendorRepository;

    public List<Vendor> getVendorDetails() throws SenderException {
        return ((List<Vendor>) vendorRepository.findAll()).parallelStream().filter(s -> s.getVendorStatus() == 1).collect(Collectors.toList());
    }


    public Vendor getVendorDetailsBasedOnName(final String vendorName) throws SenderException {
        return vendorRepository.getVendorDetailsBasedOnName(vendorName);
    }
}
