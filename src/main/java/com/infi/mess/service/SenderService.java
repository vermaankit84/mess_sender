package com.infi.mess.service;

import com.infi.mess.beans.Message;
import com.infi.mess.beans.Vendor;
import com.infi.mess.exception.SenderException;
import com.infi.mess.utilities.SmsUtilities;
import com.infi.mess.utilities.Utilities;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service("senderService")
public class SenderService {

    private final Logger logger = Logger.getLogger(SenderService.class.getName());
    @Autowired
    private VendorService vendorService = null;

    public int sendMessage(final Message messageData) throws SenderException, IOException {

        if (StringUtils.isEmpty(messageData.getVendorName())) {
            return sendDefaultVendor(messageData);
        } else {
            final String vendorName = messageData.getVendorName();
            logger.info("Vendor Name Obtained [ " + vendorName + " ] at [ " + Utilities.getCurrentDateTime() + " ] ");
            final Vendor vendors = vendorService.getVendorDetailsBasedOnName(vendorName);
            if (vendors != null) {
                final int response = SmsUtilities.getShared().getResponse(messageData, Arrays.asList(vendors));
                logger.info("Response obtained [ " + response + " ] at [ " + Utilities.getCurrentDateTime() + " ] ");
                if (response == 0) {
                    return sendDefaultVendor(messageData);
                } else {
                    return response;
                }
            } else {
                return sendDefaultVendor(messageData);
            }
        }
    }

    private int sendDefaultVendor(final Message messageData) throws SenderException, IOException {
        final List<Vendor> activeVendorList = vendorService.getVendorDetails();
        if (activeVendorList == null || activeVendorList.isEmpty()) {
            throw new SenderException("no active vendor list obtained ");
        }

        activeVendorList.parallelStream().forEach(s -> logger.info("Vendor Details obtained [ " + s + " ]  "));
        return SmsUtilities.getShared().getResponse(messageData, activeVendorList);
    }
}
