package com.infi.mess.beans;

import com.infi.mess.constants.RequestMode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "vendor")
public class Vendor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 0;

    @Column(name = "VENDOR_NAME", length = 50, unique = true, nullable = false)
    private String vendorName = null;

    @Column(name = "VENDOR_URL", nullable = false)
    private String vendorUrl = null;

    @Column(name = "VENDOR_PRIORITY", length = 11, nullable = false)
    private int vendorPriority = 0;

    @Column(name = "VENDOR_STATUS", length = 1, nullable = false)
    private int vendorStatus = 0;

    @Column(name = "VENDOR_HEADER", length = 50)
    private String vendorHeader = null;

    @Column(name = "VENDOR_CREDENTIALS", length = 45)
    private String vendorCredentials = null;

    @Column(name = "VENDOR_AUTHORIZATION" , length = 45)
    private String vendorAuthorization = null;

    @Enumerated
    @Column(name = "VENDOR_REQUEST_MODE" , length = 10)
    private RequestMode requestMode = null;

    public String getVendorAuthorization() {
        return vendorAuthorization;
    }

    public void setVendorAuthorization(String vendorAuthorization) {
        this.vendorAuthorization = vendorAuthorization;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorUrl() {
        return vendorUrl;
    }

    public void setVendorUrl(String vendorUrl) {
        this.vendorUrl = vendorUrl;
    }

    public int getVendorPriority() {
        return vendorPriority;
    }

    public void setVendorPriority(int vendorPriority) {
        this.vendorPriority = vendorPriority;
    }

    public int getVendorStatus() {
        return vendorStatus;
    }

    public void setVendorStatus(int vendorStatus) {
        this.vendorStatus = vendorStatus;
    }

    public String getVendorHeader() {
        return vendorHeader;
    }

    public void setVendorHeader(String vendorHeader) {
        this.vendorHeader = vendorHeader;
    }

    public String getVendorCredentials() {
        return vendorCredentials;
    }

    public void setVendorCredentials(String vendorCredentials) {
        this.vendorCredentials = vendorCredentials;
    }

    public RequestMode getRequestMode() {
        return requestMode;
    }

    public void setRequestMode(RequestMode requestMode) {
        this.requestMode = requestMode;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "id=" + id +
                ", vendorName='" + vendorName + '\'' +
                ", vendorUrl='" + vendorUrl + '\'' +
                ", vendorPriority=" + vendorPriority +
                ", vendorStatus=" + vendorStatus +
                ", vendorHeader='" + vendorHeader + '\'' +
                ", vendorCredentials='" + vendorCredentials + '\'' +
                ", requestMode=" + requestMode +
                '}';
    }
}
