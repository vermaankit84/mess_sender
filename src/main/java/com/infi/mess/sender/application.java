package com.infi.mess.sender;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan("com.infi")
@EnableJpaRepositories("com.infi.mess.repository")
@EnableTransactionManagement
public class application {
    public static void main(String[] args) {
        SpringApplication.run(application.class, args);
    }
}
