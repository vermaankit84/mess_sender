package com.infi.mess.template;

import org.springframework.web.client.RestTemplate;

public class SendData {

    private static volatile SendData sendData = null;
    private RestTemplate restTemplate = new RestTemplate();

    private SendData() {
    }

    public static synchronized SendData shared() {
        if (sendData == null) {
            sendData = new SendData();
        }

        return sendData;
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }
}
