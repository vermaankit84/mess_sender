package com.infi.mess.exception;


public class SenderException extends Exception {

    private static final long serialVersionUID = 1L;

    private String strException = null;

    public SenderException(final String strException) {
        this.strException = strException;
    }

    @Override
    public String toString() {
        return "SenderException{" + "strException='" + strException + '\'' + '}';
    }
}
