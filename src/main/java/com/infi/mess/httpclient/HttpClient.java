package com.infi.mess.httpclient;

import com.infi.mess.exception.SenderException;
import com.infi.mess.template.SendData;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HttpClient {

    private static HttpClient shared = new HttpClient();
    private SendData sendData = SendData.shared();

    private HttpClient() {
    }

    public static synchronized HttpClient getShared() {
        return shared;
    }

    public int getGetResponse(final String strUrl) throws SenderException, IOException {
        int response = 0;
        try {
            response =  sendData.getRestTemplate().getForEntity(new URI(strUrl) , String.class).getStatusCodeValue();
        } catch (URISyntaxException u) {

        }
        return response;
    }

    public int getPostResponse(final String strUrl, final MultiValueMap<String, String> nameValuePair, final String contentType) throws SenderException, IOException {
        final RestTemplate restTemplate = sendData.getRestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(contentType));
        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(nameValuePair, headers);
        return restTemplate.postForEntity(strUrl, request, String.class).getStatusCodeValue();
    }
}
